#include <iostream>

void number(int a, int b)
{
	for (int i = b; i <= a; i += 2) 
		std::cout << i << " ";
	std::cout << "\n";
}
int main()
{
	int a;
	std::cout << "Enter a number: ";
	std::cin >> a;

	std::cout << "Even: \n";
	number(a, 0);

	std::cout << "Odd: \n";
	number(a, 1);

	system("pause");
	return 0;
}